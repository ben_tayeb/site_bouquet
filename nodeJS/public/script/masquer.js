$(document).ready (function () {
	
	$("#composer").click(function(){
		if ( $( "#formcomposer" ).first().is( ":hidden" ) ) {
    		$( "#formcomposer" ).slideDown( "slow" );
    		$("#composer").text("Masquer son bouquet");
		} else {
    		$( "#formcomposer" ).hide("slow");
    		$("#composer").text("Composer son bouquet");
        }
	});
	
	$("#paniercommande").click(function(){
		if ( $( "#perso" ).first().is( ":hidden" ) ) {
    		$( "#perso" ).slideDown( "slow" );
    		$("#paniercommande").text("Cacher mon panier/mes commandes");
		} else {
    		$( "#perso" ).hide("slow");
    		$("#paniercommande").text("Voir mon panier/mes commandes");
        }
	});
	$("#voirCommandes").click(function(){
		if ( $( "#commande" ).first().is( ":hidden" ) ) {
    		$( "#commande" ).slideDown( "slow" );
    		$("#voirCommandes").text("Cacher mes commandes");
		} else {
    		$( "#commande" ).hide("slow");
    		$("#voirCommandes").text("Voir mes commandes");
        }
	});
	

});