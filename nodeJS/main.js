var sql = require("../db/connectdb.js");
var cors = require("cors")
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var app = express();

var corsOptions = {
	origin: '*',
  }

app.use(cors(corsOptions));

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//integre css
app.use(express.static(__dirname + '/public'));


app.get('/' , function(request, response) {
	response.sendFile(path.join(__dirname + '/public/html/connexion.html'));
})

app.get('/listeFleurs' , function(request, response) {

	sql.con.query('SELECT * FROM fleurs',function(error, results, fields) {
		response.send(results)
		
	});

})

app.get('/realiserFleurs' , function(request, response) {

	sql.con.query('SELECT * FROM Commandes WHERE pret=FALSE ',function(error, results, fields) {
		if(results.length>=1)
			response.send(results)
		else{
			response.send(null)
		}  
	});

})

app.post('/', function(request, response) {
	var mail = request.body.email;
	var mdp = request.body.motdepasse;
	if (mail && mdp) {
		sql.con.query('SELECT * FROM utilisateur WHERE email = ? AND mdp = ?',
		 				[mail, mdp], function(error, results, fields) {
			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.email = mail;
				request.session.client_id = results[0].identifiant;
				
				if(results[0].estEmployer==true){
					response.redirect('/pageEmployer.html');
				}if(results[0].estEmployer==false){
					response.redirect(('/pageAcceuilClient.html'));
				}

			} else {
				response.send('Mauvais mot de passe/email');
			}			
			response.end();
		});
	} else {
		response.send('Veuillez bien entrer mot de passe/email');
		response.end();
	}
});	

app.get('/pageAcceuilClient.html', function(request, response) {
	if (request.session.loggedin) {
		response.sendFile(path.join(__dirname + '/public/html/pageAcceuilClient.html'));
	} else {
		response.send('Connectez vous ! pour voir cette page <li><a href="connexion.html"> Se connecter </a></li> ');
		response.end();
	}
});


app.get('/pageCommandeClient.html',function(request,response){
	if (request.session.loggedin) {
		sql.con.query('SELECT * FROM Commandes WHERE clientId="'+request.session.client_id+'"',
			function(error, results, fields) {
			console.log("presqueee");
			if (error) {
				console.log(error);
			}
			response.sendFile(path.join(__dirname + '/public/html/pageCommandeClient.html'));
		});


	}else {
		response.send('Connectez vous ! pour voir cette page <li><a href="connexion.html"> Se connecter </a></li> ');
		response.end();
	}
});

app.get('/mesCommandes',function(request,response){
	if (request.session.loggedin) {
		sql.con.query('SELECT * FROM Commandes WHERE clientId="'+request.session.client_id+'"',
			function(error, results, fields) {
			console.log("presqueee");
			if (error) {
				console.log(error);
			}
			response.send(results);
			response.end()
		});


	}else {
		response.send('Connectez vous ! pour voir cette page <li><a href="connexion.html"> Se connecter </a></li> ');
		response.end();
	}
});

app.post('/maCommande', function(request,response){

	if(request.session.loggedin){
		
		var nom = request.body.nom;
		var prix = request.body.prix;
		var description = request.body.description;
		var pret = request.body.pret;
		
		
		if(nom&&prix){
			console.log("ok")
			var len =nom.length
			if(nom[0].length==1){
				if(description!=undefined && description!="undefined"){
					console.log("?");
					pret=0;
				}else{
					console.log("okk");
					console.log(pret)
					pret=1;
				}	
				sql.con.query('INSERT INTO Commandes (nom,prix,description,pret,clientId) VALUES ("'+nom+'",'+prix+',"'+description+'",'+pret+',"'+request.session.client_id+'")',
					function(error) {
					console.log("presqueee");
					if (error) {
						console.log(error);
					}
					response.redirect("/pageCommandeClient.html")
					response.end()

				});
	
			}else{
			
				for (let index = 0; index < len; index++) {
					console.log(description);

					if(description[index]!=undefined && description[index]!="undefined"){
						pret=0;
					}else{
						console.log("okk");
						console.log(pret)
						pret=1;
					}		
					sql.con.query('INSERT INTO Commandes (nom,prix,description,pret,clientId) VALUES ("'+nom[index]+'",'+prix[index]+',"'+description[index]+'",'+pret+',"'+request.session.client_id+'")',
								function(error) {
						console.log("presqueee");
						if (error) {
							console.log(error);
						}
					});
				}
				response.redirect("/pageCommandeClient.html")
				response.end()
	
			}
			
	
		}else{
			response.send("erreur");
		}

	}

});

app.post('/validerBouquetComposer',function(request,response) {
	if (request.session.loggedin) {
		var id = request.body.id_commande;
		console.log(request.body)
		console.log(request.body.id_commande)
		sql.con.query('UPDATE Commandes SET pret=TRUE WHERE id='+id,
		function(error) {
		console.log("presqueee");
		if (error) {
			console.log(error);
		}
		response.redirect("/pageEmployer.html")
		response.end()

	});
	}else {
		response.send('Connectez vous ! pour voir cette page <li><a href="connexion.html"> Se connecter </a></li> ');
		response.end();
	}
})

app.get('/pageEmployer.html',function(request,response){
	if (request.session.loggedin) {
		response.sendFile(path.join(__dirname + '/public/html/pageEmployer.html'));
	}else {
		response.send('Connectez vous ! pour voir cette page <li><a href="connexion.html"> Se connecter </a></li> ');
		response.end();
	}
});

app.get('/connexion.html',function(request,response){
	if(request.session.loggedin){
		request.session.loggedin =false;
		response.sendFile(path.join(__dirname + '/public/html/connexion.html'));
		
	}
	response.sendFile(path.join(__dirname + '/public/html/connexion.html'));
});

app.listen(8001);