drop table if exists Commandes;
drop table if exists utilisateur;
drop table if exists fleurs;

create table Commandes (id INT primary key NOAUTO_INCREMENT, nom VARCHAR(50), prix INT, description VARCHAR(250),pret boolean, clientId VARCHAR(50));
create table utilisateur (identifiant VARCHAR(50), email VARCHAR(50), mdp VARCHAR(30), estEmployer boolean) ;
create table fleurs (id INT primary key, url VARCHAR(250), prix INT,nom VARCHAR(50)) ;


-- remplissage des tables

INSERT INTO utilisateur VALUES 
("dupont",'dupont@live.fr','okok',FALSE),
("merci",'merci@live.fr','ok',TRUE),
("jean",'jean@live.fr','okokok',FALSE);

INSERT INTO fleurs VALUES 
(1,'http://localhost:8001/img_bouquet/orange.jpg',30,"Bouquet d'oranges"),
(2,'http://localhost:8001/img_bouquet/violette.jpeg',5,"Bouquet de violettes"),
(3,'http://localhost:8001/img_bouquet/tulipe.jpg',40,"Bouquet de tulipes"),
(4,'http://localhost:8001/img_bouquet/marguerite.jpg',20,"Bouquet de marguerites"),
(5,'http://localhost:8001/img_bouquet/bouquet1.jpg',25,"Bouquet de roses"),
(6,'http://localhost:8001/img_bouquet/bouquet2.jpg',15,"Bouquet 2 perso"),
(7,'http://localhost:8001/img_bouquet/bouquet3.jpg',10,"Bouquet 3 perso"),
(8,'http://localhost:8001/img_bouquet/bouquet4.jpg',30,"Bouquet 4 perso");

